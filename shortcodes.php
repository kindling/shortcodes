<?php
/**
 * Theme Shortcodes.
 *
 * @see http://codex.wordpress.org/Shortcode_API
 *
 * @package Kindling_Theme
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

if (!function_exists('add_shortcode')) {
    return;
}

/**
 * Adds a simple divider.
 * <code>[divider]</code>
 *
 * @return  string  The divider HTML.
 */
function kindling_shortcode_divider()
{
    return '<div class="divider"></div>';
}
add_shortcode('divider', 'kindling_shortcode_divider');

/**
 * Creates a 50% column
 *
 * <code>
 * [col-half]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-half]
 *
 * [col-half]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-half]
 * [/clear]
 *
 *
 * @param   array   $atts     ShortCode attributes.
 * @param   string  $content  ShortCode inner content.
 *
 * @return  string            HTML for 50% column with content.
 */
function kindling_shortcode_col_half($atts, $content)
{
    $html  = '<div class="col-sm-6 js-fitvid">';
    $html .= apply_filters('the_content', $content);
    $html .= '</div>';
    return $html;
}
add_shortcode('col-half', 'kindling_shortcode_col_half');

/**
 * Creates a 33.3333% column
 *
 * <code>
 * [col-third]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-third]
 *
 * [col-third]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-third]
 *
 * [col-third]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-third]
 * [/clear]
 * </code>
 *
 * @param   array   $atts     ShortCode attributes.
 * @param   string  $content  ShortCode inner content.
 *
 * @return  string            HTML for 33.3333% column with content.
 */
function kindling_shortcode_col_third($atts, $content)
{
    $html  = '<div class="col-sm-4 js-fitvid">';
    $html .= apply_filters('the_content', $content);
    $html .= '</div>';
    return $html;
}
add_shortcode('col-third', 'kindling_shortcode_col_third');

/**
 * Creates a 25% column
 *
 * <code>
 * [col-quarter]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-quarter]
 *
 * [col-quarter]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-quarter]
 *
 * [col-quarter]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-quarter]
 *
 * [col-quarter]
 * Curabitur faucibus non justo eu sollicitudin. Sed risus purus, volutpat.
 * [/col-quarter]
 * [/clear]
 * </code>
 *
 * @param   array   $atts     ShortCode attributes.
 * @param   string  $content  ShortCode inner content.
 *
 * @return  string            HTML for 25% column with content.
 */
function kindling_shortcode_col_quarter($atts, $content)
{
    $html  = '<div class="col-sm-3 js-fitvid">';
    $html .= apply_filters('the_content', $content);
    $html .= '</div>';
    return $html;
}
add_shortcode('col-quarter', 'kindling_shortcode_col_quarter');

/**
 * Adds a clearing div.
 *
 * <code>[clear]</code>
 *
 * @param   array   $atts     ShortCode attributes.
 *
 * @return  string            The div with .clear.
 */
function kindling_shortcode_clear($atts)
{
    $atts = extract(shortcode_atts([], $atts));

    return '<div class="clear"></div>';
}
add_shortcode('clear', 'kindling_shortcode_clear');

/**
 * Adds row shortcode.
 *
 * <code>
 * [row]
 * Content
 * [/row]
 * </code>
 *
 * @param   array  $atts    ShortCode attributes.
 * @param   string $content ShortCode inner content.
 *
 * @return string           The row HTML.
 */
function kindling_shortcode_row($atts, $content)
{
    $content = apply_filters('the_content', $content);
    return "<div class='row'>{$content}</div>";
}
add_shortcode('row', 'kindling_shortcode_row');
